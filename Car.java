package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Car {
    public String carBrand;
    public String carClass;
    public int weight;
    Driver driver = new Driver();
    Engine engine = new Engine();
    public Car(String carBrand, String carClass, int weight){
        this.engine=new Engine();
        this.driver=new Driver();
        this.carClass=carClass;
        this.carBrand=carBrand;
        this.weight=weight;}

    public Car() {}

    public Driver getDriver() {return driver;}
    public Engine getEngine() {return engine;}
    public int getWeight() {return weight;}
    public String getCarBrand() {return carBrand;}
    public String getCarClass() {return carClass;}
    public void setCarBrand(String carBrand) {this.carBrand = carBrand;}
    public void setCarClass(String carClass) {this.carClass = carClass;}
    public void setDriver(Driver driver) {this.driver = driver;}
    public void setEngine(Engine engine) {this.engine = engine;}
    public void setWeight(int weight) {this.weight = weight;}
    public void start(){
        System.out.println("ПоЇхали");
    }
    public void stop(){
        System.out.println("Зупиняємося");
    }
    public void turnRight(){
        System.out.println("Поворот направо");
    }
    public void turnLeft(){
        System.out.println("Поворот наліво");
    }
    public String toString(){
        return "Car:"
                + "car brand: " + carBrand
        + " Class: " + carClass
                + " Driver: " + driver
                + " Engine: " + engine
                ;
    }
}

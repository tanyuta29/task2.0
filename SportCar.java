package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class SportCar extends Car{
    private int speed;
    public SportCar(int speed) {
        this.speed=speed;
    }
    public int getSpeed() {return speed;}
    public void setSpeed(int speed) {this.speed = speed;}
    public Driver getDriver() {return super.getDriver();}
    public Engine getEngine() {return super.getEngine();}
    public int getWeight() {return super.getWeight();}
    public String getCarBrand() {return super.getCarBrand();}
    public String getCarClass() {return super.getCarClass();}
    public void setCarBrand(String carBrand) {super.setCarBrand(carBrand);}
    public void setCarClass(String carClass) {super.setCarClass(carClass);}
    public void setDriver(Driver driver) {super.setDriver(driver);}
    public void setEngine(Engine engine) {super.setEngine(engine);}
    public void setWeight(int weight) {super.setWeight(weight);}
    public String toString() {
        return super.toString()
                + "Speed: "+ getSpeed();
    }
}

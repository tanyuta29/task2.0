package com.company.professions;

import com.company.entities.Person;

public class Driver extends Person {
    private String drivingExperience;
    public Driver(String drivingExperience){
        this.drivingExperience=drivingExperience;
    }
    public Driver(){
        this.drivingExperience=drivingExperience;}
    public String getDrivingExperience() {return drivingExperience;}
    public void setDrivingExperience(String drivingExperience) {this.drivingExperience = drivingExperience;}
    public int getAge() {return super.getAge();}
    public int getPhoneNumber() {return super.getPhoneNumber();}
    public String getGender() {return super.getGender();}
    public String getNameSurname() {return super.getNameSurname();}
    public String toString() {return super.toString();}
    public void setAge() {super.setAge();}
    public void setGender() {super.setGender();}
    public void setNameSurname() {super.setNameSurname();}
    public void setPhoneNumber() {super.setPhoneNumber();}
}

